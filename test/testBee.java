
import bee_simulator.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class testBee {

    RandomBee randomBee;

    @BeforeEach
    public void setUp() throws IOException {
        randomBee = new RandomBee(0,0);

        randomBee.setTargetCoords(100,100);
    }

    @Test
    public void randomBeeMoveTest() {
        randomBee.move();
        Assertions.assertEquals(5, randomBee.getLocationX());
        Assertions.assertEquals(5, randomBee.getLocationY());

        randomBee.setTargetCoords(0,0);
        randomBee.move();
        Assertions.assertEquals(0, randomBee.getLocationX());
        Assertions.assertEquals(0, randomBee.getLocationY());

        randomBee.setTargetCoords(10,0);
        randomBee.move();
        Assertions.assertEquals(10, randomBee.getLocationX());
        Assertions.assertEquals(0, randomBee.getLocationY());

        randomBee.setTargetCoords(0,0);
        randomBee.move();
        Assertions.assertEquals(0, randomBee.getLocationX());
        Assertions.assertEquals(0, randomBee.getLocationY());

        randomBee.setTargetCoords(0,10);
        randomBee.move();
        Assertions.assertEquals(0, randomBee.getLocationX());
        Assertions.assertEquals(10, randomBee.getLocationY());

        randomBee.setTargetCoords(0,0);
        randomBee.move();
        Assertions.assertEquals(0, randomBee.getLocationX());
        Assertions.assertEquals(0, randomBee.getLocationY());


        randomBee.setTargetCoords(20,80);
        randomBee.move();
        Assertions.assertEquals(2, randomBee.getLocationX());
        Assertions.assertEquals(8, randomBee.getLocationY());

    }


}
