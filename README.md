# SE2811 Beekeepers


## Name
Lab 4- Bees cont.

## Description
Runs from bee_simulator/src/bee_simulator/Main, code which was provided in the lab description. Displays
four main entities- ChargeFlowers, which provide bees with energy, DrainFlowers, which drain bees of energy,
RandomBees, which move in the direction of a random flower, and PatternBees, which move across the garden 
in a pattern.

Over time, bees will die from lack of energy. If all your bees die, try changing the garden. Adding 
ChargeFlowers and removing DrainFlowers can help bees live longer!

## To Use
To simulate garden activity, press the right arrow key. Each press will "tick" the garden. Each tick will charge a 
charge flower, drain a drain flower, and drain and move bees according to their movement style. 

To add flowers and bees, click the first button in the side panel that is next to the name of your preferred entity. 
One entity will be randomly placed into the garden for each click. To remove them, click the second button.
 