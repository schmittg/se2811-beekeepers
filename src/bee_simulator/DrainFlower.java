/*
 * Course:     SE 2811 031
 * Term:       Winter 2021-22
 * Assignment: Lab 4
 * Author:     Grace Schmitt
 * Date:       1/4/2022
 */

package bee_simulator;

import javafx.scene.image.Image;


public class DrainFlower extends Flower {

    private static final Image FLOWER_IMAGE = new Image("garden_jpgs/nightshade.jpg");

    private static final String DESCRIPTION = "DrainFlower: Drains a small amount of energy from a bee, as its own " +
            "energy decreases over time.";


    public DrainFlower(int locationX, int locationY) {
        super(locationX, locationY, FLOWER_IMAGE);
    }

    @Override
    public void increaseEnergy() {
        if(getEnergy() > 0) {
            setEnergy(getEnergy() - 1);
        }
        displayEnergy();
    }

    @Override
    public int drawEnergy() {
        if(getEnergy() < getMaxEnergy() / 2) {
            int energyDrawn = 5;
            setEnergy(getEnergy() + energyDrawn);
            displayEnergy();
            //Return negative energy- indicate the bee loses energy when touching this flower
            return -energyDrawn;
        }
        return 0;

    }

    public static String getDescription() {
        return DESCRIPTION;
    }
}
