/*
 * Course:      SE 2811
 * Term:        Winter 2021 - 2022
 * Assignment:  Lab 4 - Bees
 * Author:      Abigail Draper
 * Date:        1/4/2022
 */

package bee_simulator;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.util.LinkedList;
import java.util.List;

/**
 * The Flower class is responsible for storing a flower's location and energy level,
 * as well as displaying the image and energy status of the flower.
 */
public abstract class Flower {

    private final int locationX;
    private final int locationY;

    private int energy;
    private static final int MAX_ENERGY = 50;
    private final ImageView view;

    private static int flowerSize = 50;

    private final List<Node> guiElements;

    private HealthBar healthBar;

    public Flower(int locationX, int locationY, Image flowerImage) {
        this.locationX = locationX;
        this.locationY = locationY;

        energy = MAX_ENERGY;

        // init GUI components
        guiElements = new LinkedList<>();

        view = new ImageView(flowerImage);
        guiElements.add(view);
        displayEnergy();
        displayFlower();

    }

    public abstract void increaseEnergy();

    public int getLocationX() {
        return locationX;
    }

    public int getLocationY() {
        return locationY;
    }

    public static int getWidth() {
        return flowerSize;
    }

    public static int getHeight() {
        return flowerSize;
    }

    public abstract int drawEnergy();

    public void displayFlower() {
        view.setX(locationX);
        view.setY(locationY);
        view.setFitWidth(flowerSize);
        view.setFitHeight(flowerSize);
        healthBar.displayHealthBar(locationX, locationY, energy);
    }

    public void displayEnergy() {
        if(healthBar == null) {
            healthBar = HealthBar.createHealthBar(MAX_ENERGY, this);
            guiElements.addAll(healthBar.getGUIElements());
        }
        healthBar.displayHealthBar(locationX, locationY, energy);
    }

    // Getter for adding to the Pane's children
    public List<Node> getGUIElements() {
        return guiElements;
    }

    public int getMaxEnergy() {
        return MAX_ENERGY;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public static void setFlowerSize(int size) {
        flowerSize = size;
    }

}
