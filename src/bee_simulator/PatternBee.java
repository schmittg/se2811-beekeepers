/*
 * Course:      SE 2811
 * Term:        Winter 2021 - 2022
 * Assignment:  Lab 4 - Bees
 * Author:      Abigail Draper
 * Date:        1/4/2022
 */

package bee_simulator;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * The PatternBee class is responsible for controlling a bee that moves in straight lines back and forth,
 * covering the whole garden.
 */
public class PatternBee extends Bee {

    // Distance moved with each tick
    private static final int moveIncrement = 40;

    // Temporary values -- will link to pane and imageview later
    private static int paneWidth = 600;
    private static int paneHeight = 600;
    private static int beeSize = 50;

    private static final Image PATTERN_BEE_IMAGE = new Image("garden_jpgs/bee-3.jpg");

    private HealthBar healthBar;

    public List<Node> guiElements;

    private boolean movedSecondary;
    
    private final int[] dir;
    private final int primaryDir;

    private static final String DESCRIPTION = "PatternBee: Moves back and forth in lines, covering the garden.";

    public PatternBee(int locationX, int locationY) {
        super(locationX, locationY, PATTERN_BEE_IMAGE);

        Random random = new Random();

        // Randomize the primary direction.
        primaryDir = random.nextInt(2);

        // Randomly generates either -1 or 1 for both the starting X and Y directions.
        dir = new int[] {(int)Math.pow(-1, random.nextInt(2)),
                (int) Math.pow(-1, random.nextInt(2))};

        displayBee();

    }

    @Override
    public void move() {
        int[] move = spacesMoved();
        setLocationX(this.getLocationX() + move[0]);
        setLocationY(this.getLocationY() + move[1]);
        //Author: Grace Schmitt
        setHealth(getHealth() - 1);
        if(getHealth() <= 0) {
            die();
        }
        displayBee();
    }

    private int[] spacesMoved() {
        int[] spacesMoved = new int[2];
        int primaryCoord = primaryDir == 0 ? getLocationX() : getLocationY();
        int secondaryCoord = primaryDir == 0 ? getLocationY() : getLocationX();
        if(Math.abs(moveDistance(primaryCoord, primaryDir)) > 0) {
            spacesMoved[primaryDir] += moveDistance(primaryCoord, primaryDir);
        } else {
            if(movedSecondary) {
                dir[primaryDir] *= -1;
            }
            if(Math.abs(moveDistance(secondaryCoord, (primaryDir + 1) % 2)) == 0) {
                dir[(primaryDir + 1) % 2] *= -1;
            }
            movedSecondary = !movedSecondary;
            spacesMoved[(primaryDir + 1) % 2] += moveDistance(secondaryCoord, (primaryDir + 1) % 2);
        }
        return spacesMoved;
    }

    private int moveDistance(int location, int dirIndex) {
        int maxBound = dirIndex == 0 ? paneHeight - beeSize : paneWidth - beeSize;
        int minBound = dirIndex == 0 ? 0 : 15;
        int distance = moveIncrement * dir[dirIndex];
        if(location + distance < minBound) {
            distance = minBound - location;
        } else if (location + distance > maxBound) {
            distance = maxBound - location;
        }
        return distance;
    }


    public static Image getPatternBeeImage() {
        return PATTERN_BEE_IMAGE;
    }

    public static String getDescription() {
        return DESCRIPTION;
    }

    public static void setBeeSize(int size) {
        beeSize = size;
    }

    public static void setPaneWidth(int width) {
        paneWidth = width;
    }

    public static void setPaneHeight(int height) {
        paneHeight = height;
    }



}
