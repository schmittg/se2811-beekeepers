/*
 * Course:     SE 2811 031
 * Term:       Winter 2021-22
 * Assignment: Lab 4
 * Author:     Grace Schmitt
 * Date:       1/4/2022
 */
package bee_simulator;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.LinkedList;
import java.util.List;

/**
 *The RandomBee class is responsible for controlling a bee which will move in a straight line, directed towards
 * a targeted flower.
 *
 */
public class RandomBee extends Bee {

    private Flower target;
    private static final Image RANDOM_BEE_IMAGE = new Image("garden_jpgs/bee-1.jpg");


    private static final String DESCRIPTION = "RandomBee: Selects a random flower and moves to it in a straight line.";


    public RandomBee(int locationX, int locationY) {
        super(locationX, locationY, RANDOM_BEE_IMAGE);

        displayBee();

    }

    @Override
    public void move() {

        //get flower location, move towards it in a straight line
        double moveLength = 10.0;
        int xToMove = this.getLocationX() - target.getLocationX();
        int yToMove = this.getLocationY() - target.getLocationY();
        double ratio = Math.abs((double) xToMove/ (double) (Math.abs(xToMove) +  Math.abs(yToMove)));
        int xMove = (int) (ratio * moveLength);
        int yMove = (int) moveLength - xMove;
        setLocationX(getNewLocation(getLocationX(), xToMove, xMove));
        setLocationY(getNewLocation(getLocationY(), yToMove, yMove));
        setHealth(getHealth() - 1);
        if(getHealth() <= 0) {
            die();
        }
        displayBee();
    }

    private int getNewLocation(int location, int toMove, int moving) {
        if(toMove < 0) {
            return location + moving;
        } else {
            return location - moving;
        }
    }

    public void setTarget(Flower flower) {
        this.target = flower;
    }

    public Flower getTarget(){
        return target;
    }

    //TODO: used to test movement. delete when done testing
    public void setTargetCoords(int x, int y) {
        target = new ChargeFlower(x, y);
    }


    public static String getDescription() {
        return DESCRIPTION;
    }

}
