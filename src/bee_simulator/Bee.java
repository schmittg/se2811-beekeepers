/*
 * Course:     SE 2811 031
 * Term:       Winter 2021-22
 * Assignment: Lab 4
 * Author:     Grace Schmitt
 * Date:       1/4/2022
 */

package bee_simulator;


import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.LinkedList;
import java.util.List;

public abstract class Bee {
    private int health;
    private boolean isDead;
    private int locationX;
    private int locationY;

    private final ImageView beeImageView;

    private static final int beeSize = 50;

    private final List<Node> guiElements;

    private HealthBar healthBar;

    private static final int MAX_HEALTH = 50;


    public Bee(int locationX, int locationY, Image beeImage) {
        isDead = false;
        this.locationX = locationX;
        this.locationY = locationY;
        health = MAX_HEALTH;

        // Author: Abigail Draper
        guiElements = new LinkedList<>();
        beeImageView = new ImageView(beeImage);
        guiElements.add(beeImageView);
    }

    public abstract void move();

    public void collide(Bee bee) {
        if(!bee.isDead){
            health -= 2;
            if(health < 0) {
                die();
            }
            displayBee();
        }
    }

    public void touchFlower(Flower flower) {
        health += flower.drawEnergy();
        if(health > MAX_HEALTH) {
            health = MAX_HEALTH;
        } else if (health < 0) {
            die();
        }
        displayHealth();
    }

    public void die() {
        health = 0;
        isDead = true;
        beeImageView.setVisible(false);
        healthBar.removeHealthBar();
        displayBee();
    }

    // Author: Abigail Draper
    public void displayBee() {
        beeImageView.setFitWidth(beeSize);
        beeImageView.setFitHeight(beeSize);
        beeImageView.setX(locationX);
        beeImageView.setY(locationY);
        displayHealth();
    }

    // Author: Abigail Draper
    private void displayHealth() {
        if(healthBar == null) {
            healthBar = HealthBar.createHealthBar(50, this);
            guiElements.addAll(healthBar.getGUIElements());
        }
        healthBar.displayHealthBar(locationX, locationY, health);
    }

    // Author: Abigail Draper
    public List<Node> getGUIElements() {
        return guiElements;
    }

    public boolean isDead() {
        return isDead;
    }


    public int getLocationX() {
        return locationX;
    }

    public int getLocationY() {
        return locationY;
    }

    public void setLocationX(int x) {
        this.locationX = x;
    }

    public void setLocationY(int y) {
        this.locationY = y;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getHealth() {
        return health;
    }
}
