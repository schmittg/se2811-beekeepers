/*
 * Course:     SE 2811 031
 * Term:       Winter 2021-22
 * Assignment: Lab 4
 * Author:     Grace Schmitt, Abigail Draper
 * Date:       1/4/2022
 */

package bee_simulator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Point2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("bee_simulator.fxml"));
        Parent root = loader.load();
        GardenController controller = loader.getController();
        controller.initFlowers(10);
        controller.initBees(10);
        controller.setPaneSize();
        primaryStage.setTitle("Bee Runner 2049");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public Main() {
    }

    public static void main(String[] args) {
        launch(args);
    }
}
