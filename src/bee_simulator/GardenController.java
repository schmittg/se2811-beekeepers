/*
 * Course:     SE 2811 031
 * Term:       Winter 2021-22
 * Assignment: Lab 4
 * Author:     Grace Schmitt, Abigail Draper
 * Date:       1/4/2022
 */

package bee_simulator;

import javafx.fxml.FXML;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

import java.util.Random;

public class GardenController {

    private Pane beeImageBox;               // box containing bee and it's label; NOT a good domain name!
    private double beeXLocation, beeYLocation;  // drawn location of bee; this should be in a domain class
    private Garden garden;

    private static final Random RANDOM = new Random();

    @FXML
    private Pane theGarden;                 // capture the pane we are drawing on from JavaFX

    @FXML
    private ImageView chargeFlowerKey;
    @FXML
    private ImageView drainFlowerKey;
    @FXML
    private ImageView randomBeeKey;
    @FXML
    private ImageView patternBeeKey;

    @FXML
    private Tooltip chargeFlowerDesc;
    @FXML
    private Tooltip drainFlowerDesc;
    @FXML
    private Tooltip randomBeeDesc;
    @FXML
    private Tooltip patternBeeDesc;

    private static final int FLOWER_OFFSET = 50;
    private static final int OBJECT_SIZE = 35;


    @FXML
    public void initialize() {              // executed after scene is loaded but before any methods
        // for fun, set up a gradient background; could probably do in SceneBuilder as well
        // note the label has a Z index of 2 so it is drawn above the panel, otherwise it may be displayed "under" the panel and not be visible
        theGarden.setStyle("-fx-background-color: linear-gradient(to bottom right, derive(forestgreen, 20%), derive(forestgreen, -40%));");
        // load image from a file; the file needs to be in the top folder of the project
        garden = new Garden(this);
        legendSetup();
        theGarden.setFocusTraversable(true); // ensure garden pane will receive keypresses
    }


    @FXML
    public void onKeyPressed(KeyEvent keyEvent) {

        onTick(keyEvent);
    }

    @FXML
    private void addChargeFlower() {
        addFlower(0);
    }

    @FXML
    private void addDrainFlower() {
        addFlower(1);
    }

    @FXML
    private void removeChargeFlower() {
        removeFlower(0);
    }

    @FXML
    private void removeDrainFlower() {
        removeFlower(1);
    }


    @FXML
    private void addPatternBee() {
        addBee(0);
    }

    @FXML
    private void addRandomBee() {
        addBee(1);
    }

    @FXML
    private void removePatternBee() {
        removeBee(0);
    }

    @FXML
    private void removeRandomBee() {
        removeBee(1);
    }

    public void initFlowers(int numFlowers) {
        // Thinking numFlowers will make half ChargeFlower and half DrainFlower-- can change this later
        for(int i = 0; i < numFlowers / 2; i++) {
            addFlower(0);
        }
        for(int i = numFlowers / 2; i < numFlowers; i++) {
            addFlower(1);
        }
    }

    public void initBees(int numBees) {
        for(int i = 0; i < numBees / 2; i++) {
            addBee(0);
        }
        for(int i = numBees / 2; i < numBees; i++) {
            addBee(1);
        }
    }

    private void displayFlowers() {
        for(Flower f : garden.flowers) {
            f.displayFlower();
        }
    }

    private void addFlower(int type) {
        // Type: 0 for ChargeFlower, 1 for DrainFlower
        int[] location = generateFlowerLocation();
        if(location[0] >= 0) {
            Flower flower = type == 0 ? new ChargeFlower(location[0], location[1]) :
                    new DrainFlower(location[0], location[1]);
            garden.addFlower(flower);
            theGarden.getChildren().addAll(flower.getGUIElements());
        }
    }

    private void addBee(int type) {
        //0 for PatternBee, 1 for RandomBee
        int[] location = generateFlowerLocation();
        if(location[0] >= 0) {
            Bee bee = new PatternBee(location[0], location[1]);
            if(type == 1) {
                bee = new RandomBee(location[0], location[1]);
                //If bee is a randomBee it will need a target flower
                int targetFlower = (int) (Math.random() * garden.flowers.size());
                ((RandomBee) bee).setTarget(garden.flowers.get(targetFlower));
            }
            garden.addBee(bee);
            theGarden.getChildren().addAll(bee.getGUIElements());
        }
    }

    private void removeFlower(int type) {
        // Type: 0 for ChargeFlower, 1 for DrainFlower
        int counter = garden.flowers.size() - 1;
        do {
            Flower flower = garden.flowers.get(counter);
            if((type == 0 && flower instanceof ChargeFlower) ||
                    (type == 1 && flower instanceof DrainFlower)) {
                removeFlower(flower);
                counter = -1;
            } else {
                counter--;
            }
        } while(counter >= 0);
    }

    private void removeBee(int type) {
        // Type: 0 for PatternBee, 1 for RandomBee
        int counter = garden.bees.size() - 1;
        do {
            Bee bee= garden.bees.get(counter);
            if((type == 0 && bee instanceof PatternBee) ||
                    (type == 1 && bee instanceof RandomBee)) {
                removeBee(bee);
                counter = -1;
            } else {
                counter--;
            }
        } while(counter >= 0);
    }


    private void removeFlower(Flower flower) {
        garden.flowers.remove(flower);
        theGarden.getChildren().removeAll(flower.getGUIElements());
    }

    public void removeBee(Bee bee) {
        garden.bees.remove(bee);
        theGarden.getChildren().removeAll(bee.getGUIElements());
    }

    // May make this part of the flower class itself?
    private int[] generateFlowerLocation() {
        int flowerX, flowerY;
        int numTries = 0;

        do {
            flowerX = RANDOM.nextInt(600);
            flowerY = RANDOM.nextInt(600);
            numTries++;
        } while(numTries < 500 && isOverlappingFlower(flowerX, flowerY) ||
                isOutOfBounds(flowerX, flowerY));

        if(numTries == 500) {
            System.out.println("Too many flowers!!!");
            return new int[] {-1, -1};
        }

        return new int[] {flowerX, flowerY};
    }

    private boolean isOverlappingFlower(int flowerX, int flowerY) {
        boolean overlap = false;
        for(int i = 0; i < garden.flowers.size() && !overlap; i++) {
            Flower f = garden.flowers.get(i);
            overlap = Math.abs(flowerX - f.getLocationX()) < Flower.getWidth() + FLOWER_OFFSET &&
                    Math.abs(flowerY - f.getLocationY()) < Flower.getHeight() + FLOWER_OFFSET;
        }
        return overlap;
    }

    private boolean isOutOfBounds(int flowerX, int flowerY){
        int offset = 5;
        int energyOffset = 15;

        return flowerX < 0 ||
                flowerX + Flower.getWidth() + offset > 600 ||
                flowerY - energyOffset < 0 ||
                flowerY + Flower.getHeight() + offset > 600;
    }

    private void legendSetup() {
        chargeFlowerKey.setImage(ChargeFlower.getFlowerImage());
        drainFlowerKey.setImage(new Image("garden_jpgs/nightshade.jpg"));
        randomBeeKey.setImage(new Image("garden_jpgs/bee-1.jpg"));
        patternBeeKey.setImage(PatternBee.getPatternBeeImage());
        chargeFlowerDesc.setText(ChargeFlower.getDescription());
        drainFlowerDesc.setText(DrainFlower.getDescription());
        randomBeeDesc.setText(RandomBee.getDescription());
        patternBeeDesc.setText(PatternBee.getDescription());
    }

    @FXML
    private void onTick(KeyEvent k) {
        if(k.getCode().equals(KeyCode.RIGHT)) {
            garden.chargeFlowers();
            garden.moveBees();
            garden.checkForCollisions();
            displayFlowers();
        }
    }

    public void setObjectSize(int size) {
        PatternBee.setBeeSize(size);
        Flower.setFlowerSize(size);
        HealthBar.setHealthBarWidth(size);
    }

    public void setPaneSize() {
        PatternBee.setPaneWidth(600);
        PatternBee.setPaneHeight(600);
    }
}
