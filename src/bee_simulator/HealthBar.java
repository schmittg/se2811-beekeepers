/*
 * Course:      SE 2811
 * Term:        Winter 2021 - 2022
 * Assignment:  Lab 4 - Bees
 * Author:      Abigail Draper
 * Date:        1/4/2022
 */

package bee_simulator;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.LinkedList;
import java.util.List;

/**
 * The HealthBar class is responsible for managing the GUI elements related to an object's energy status.
 */
public class HealthBar {

    private final Rectangle healthContainer, healthLevel;
    private final Label healthLabel;

    private static int healthBarWidth = 50;
    private static final int HEALTH_BAR_HEIGHT = 10;

    private static final Color HEALTH_LEVEL_COLOR = Color.DARKRED;
    private static final Color HEALTH_CONTAINER_COLOR = Color.LIGHTGRAY;

    private final List<Node> guiElements;
    private final int maxEnergy;

    private static int labelOffset = healthBarWidth + 10;
    private static final int yOffset = 15;

    public HealthBar(int maxEnergy) {
        this.maxEnergy = maxEnergy;
        healthContainer = new Rectangle(healthBarWidth, HEALTH_BAR_HEIGHT, HEALTH_CONTAINER_COLOR);
        healthLevel = new Rectangle(healthBarWidth, HEALTH_BAR_HEIGHT, HEALTH_LEVEL_COLOR);
        healthLabel = new Label(maxEnergy + "");

        guiElements = new LinkedList<>();
        guiElements.add(healthContainer);
        guiElements.add(healthLevel);
        guiElements.add(healthLabel);
    }

    public void displayHealthBar(int locationX, int locationY, int energy) {
        updateLocation(locationX, locationY);
        healthContainer.setWidth(healthBarWidth);
        healthLevel.setWidth((double) energy / maxEnergy * healthBarWidth);
        healthLabel.setText(energy + "");

    }

    /**
     * Used when a bee dies and no longer needs its heath to be tracked
     */
    public void removeHealthBar() {
        healthContainer.setVisible(false);
        healthLabel.setVisible(false);
    }

    private void updateLocation(int locationX, int locationY) {
        healthContainer.setX(locationX);
        healthContainer.setY(locationY - yOffset);

        healthLevel.setX(locationX);
        healthLevel.setY(locationY - yOffset);

        healthLabel.setLayoutX(locationX + labelOffset);
        healthLabel.setLayoutY(locationY - yOffset);
    }

    public List<Node> getGUIElements() {
        return guiElements;
    }

    // Static methods for creating health bars so we can pass in locations
    // so they won't appear in the wrong spot before movement.
    public static HealthBar createHealthBar(int maxEnergy, Bee bee) {
        HealthBar healthBar = null;
        if(bee != null) {
            healthBar = new HealthBar(maxEnergy);
            healthBar.updateLocation(bee.getLocationX(), bee.getLocationY());
        }
        return healthBar;
    }

    public static HealthBar createHealthBar(int maxEnergy, Flower flower) {
        HealthBar healthBar = null;
        if(flower != null) {
            healthBar = new HealthBar(maxEnergy);
            healthBar.updateLocation(flower.getLocationX(), flower.getLocationY());
        }
        return healthBar;
    }

    public static void setHealthBarWidth(int width) {
        healthBarWidth = width;
        labelOffset = width + 10;
    }
}
