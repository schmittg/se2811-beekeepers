/*
 * Course:     SE 2811 031
 * Term:       Winter 2021-22
 * Assignment: Lab 4
 * Author:     Grace Schmitt
 * Date:       1/4/2022
 */

package bee_simulator;

import java.util.ArrayList;

public class Garden {
    private final GardenController GC;

    public ArrayList<Bee> bees;

    public ArrayList<Flower> flowers;

    public Garden(GardenController GC) {
        this.GC = GC;
        bees = new ArrayList<>();
        flowers = new ArrayList<>();
    }

    public void addBee(Bee bee) {
        bees.add(bee);
    }

    //ensures the GUI components of bees are removed
    public void removeBee(Bee bee) {
        bees.remove(bee);
        GC.removeBee(bee);
    }

    public void addFlower(Flower flower) {
        flowers.add(flower);
    }

    public void moveBees(){
        for (Bee bee: bees) {
            bee.move();
        }
    }

    public void chargeFlowers(){
        for (Flower flower: flowers) {
            flower.increaseEnergy();
        }

    }

    public void checkForCollisions(){
        //check if any bee or flower locations overlap
        ArrayList<Bee> deadBees = new ArrayList<>();
        for (Bee bee: bees) {
            int beeX = bee.getLocationX();
            int beeY = bee.getLocationY();
            boolean hasBumped = false;
            for(int i = 0; i < bees.size() && !hasBumped; i++) {
                Bee bee2 = bees.get(i);
                // if bees are very near to each other and are not the same bee,
                // they collide (and are removed from garden if they die)
                if(Math.abs(beeX - bee2.getLocationX()) < 20 && Math.abs(beeY - bee2.getLocationY()) < 20 && bee != bee2) {
                    bee.collide(bee2);
                    hasBumped = true;
                }
            }

            for(Flower flower: flowers) {
                if(Math.abs(beeX - flower.getLocationX()) < 20 && Math.abs(beeY - flower.getLocationY()) < 20) {
                    bee.touchFlower(flower);
                    try{
                        Flower target = ((RandomBee) bee).getTarget();
                        //if bee has reached the target flower, select a new target for the bee
                        if(target == flower) {
                            ((RandomBee) bee).setTarget(flowers.get((int) (Math.random() * flowers.size())));
                        }
                    } catch (ClassCastException e) {
                        //do nothing
                    }
                }
            }
            if(bee.isDead()) {
                deadBees.add(bee);
            }
        }
        for (Bee deadBee:deadBees) {
            removeBee(deadBee);
        };

    }

}
