/*
 * Course:      SE 2811
 * Term:        Winter 2021 - 2022
 * Assignment:  Lab 4 - Bees
 * Author:      Abigail Draper
 * Date:        1/4/2022
 */

package bee_simulator;

import javafx.scene.image.Image;


/**
 * The ChargeFlower class is responsible for representing a flower that gives bees energy
 * and recharges energy between bee visits.
 */
public class ChargeFlower extends Flower {


    private static final Image FLOWER_IMAGE = new Image("garden_jpgs/aster.jpg");
    private static final String DESCRIPTION = "Charge Flower: Gives a bee energy," +
            " then must recharge over the next few ticks.";

    public ChargeFlower(int locationX, int locationY) {
        super(locationX, locationY, FLOWER_IMAGE);
    }

    @Override
    public void increaseEnergy() {
        int newEnergy = getEnergy() + 1;
        if(newEnergy > getMaxEnergy()) {
            newEnergy = getMaxEnergy();
        }
        setEnergy(newEnergy);
        displayEnergy();
    }

    @Override
    public int drawEnergy() {
        int energyTransferred = 0;
        if(getEnergy() >= getMaxEnergy() / 2) {
            energyTransferred += 15;
        }
        setEnergy(getEnergy() - energyTransferred);
        displayEnergy();
        return energyTransferred;

    }

    public static String getDescription() {
        return DESCRIPTION;
    }

    public static Image getFlowerImage() {
        return FLOWER_IMAGE;
    }

}
